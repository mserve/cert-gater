// Load config
require('dotenv').config();

// Node imports
var express = require('express');
var fs = require('fs');

var http = require('http');
var bodyParser = require('body-parser');
var ipRangeCheck = require("ip-range-check");



// Load JSON config
var config = JSON.parse(fs.readFileSync(process.env.APP_CONFIG));


var app = express(); // Create our app with express
app.use(bodyParser.urlencoded({
    'extended': 'true'
})); // Parse application/x-www-form-urlencoded
app.use(bodyParser.json()); // Parse application/json
app.use(bodyParser.json({
    type: 'application/vnd.api+json'
})); // Parse application/vnd.api+json as json

// Listen (start app with node server.js)
var options = {
//    key: fs.readFileSync(process.env.APP_KEY),
//    cert: fs.readFileSync(process.env.APP_CERT)
};

// Trust proxies
app.set('trust proxy', 'loopback');

// Fire HTTPS server
var server = http.createServer(options, app).listen(process.env.APP_PORT, "0.0.0.0");
console.log("[HTTP] App listening on http://" + process.env.APP_URL + ':' + process.env.APP_PORT + "/");

app.get('/.well-known/acme-challenge/:token',     getChallengeController);
app.get('/acme-challenge/new/:authtoken/:token/:challenge',   newChallengeController);
app.get('/acme-challenge/delete/:authtoken/:token',          deleteChallengeController);

var mapChallenges = {};

function getChallengeController(req, res) {
	if (mapChallenges[req.params.token])
	{
		res.send(mapChallenges[req.params.token]);
		return;
	}
	else
	{
		res.status(404).send('Sorry cant find that token!');
		return;
	}
}

function newChallengeController(req, res) {
        var ip = req.ip; //req.headers['x-forwarded-for'] || req.ip;
	if (!!req.params.authtoken && isIpAllowed(ip, req.params.authtoken))
	{
		if (!!req.params.token && !! req.params.challenge)
		{
			mapChallenges[req.params.token] = req.params.challenge;
			res.status(200).send('ok');
			return;
		} else {
			res.status(400).send('check parameters');
			return;
		}
	} else {
		res.status(403).send('IP ' + ip + ' not authorised');
		return;
	}
}

function deleteChallengeController(req, res) {
        var ip = req.ip; //req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        if (!!req.params.authtoken && isIpAllowed(ip, req.params.authtoken))
	{
		if (!!req.params.token)	{
			if (mapChallenges[req.params.token]) {
				delete mapChallenges[req.params.token];
				res.status(200).send('ok');
				return;
			} else {
				res.status(404).send('token not found');
				return;
			}
		}
		else {
                        res.status(400).send('check parameters');
			return;
		}
        } else {
                res.status(403).send('IP ' + ip + ' not authorised');
		return;
        }
}


var checkAuth = function (req, res, next) {
  var ip = req.ip; //req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  if (!!req.params.authtoken && isIpAllowed(ip, req.params.authtoken))
	  next();
  else
    res.status(403).send('IP ' + ip + ' not authorised');
};


var isIpAllowed = function (ip, token) {
	// Strip of ::ffff: if IPv4 used
	if (ip.substr(0, 7) == "::ffff:") {
 		 ip = ip.substr(7)
	}
	var tokenValid = false;
	config.domains.forEach(function (domain) {
		if ((domain.token === token) && ipRangeCheck(ip, domain.ipList))
		{
			tokenValid = true;
			return;
		}
	});
	return tokenValid;
};

